# Testing workshop

Repository for the testing workshop. It's mostly a copy of [TodoMVC React](https://github.com/tastejs/todomvc/tree/gh-pages/examples/react) with a few tweaks. Don't focus on the actual code, it wasn't written with good practices in mind since it's not the focus of the workshop.

## Resources

- [Write tests. Not too many. Mostly integration.](https://kentcdodds.com/blog/write-tests)
- [Testing implementation details](https://kentcdodds.com/blog/testing-implementation-details)
- [Colocation](https://kentcdodds.com/blog/colocation)
- [Common mistakes with RTL](https://kentcdodds.com/blog/common-mistakes-with-react-testing-library)
- [Avoid nesting when you're testing](https://kentcdodds.com/blog/avoid-nesting-when-youre-testing)

## Libraries

- [@testing-library/react](https://testing-library.com/docs/react-testing-library/intro/)
- [@testing-library/user-event](https://github.com/testing-library/user-event)
- [fetch-mock](http://www.wheresrhys.co.uk/fetch-mock/)
- [react-query](https://react-query.tanstack.com/)
