const fastify = require('fastify')({
  logger: true,
})

fastify.register(require('fastify-cors'), {
  origin: '*',
})

const db = new Map()

db.set('first', buildTodo({ id: 'first', title: 'Testing workshop' }))
db.set('second', buildTodo({ id: 'second', title: 'Beers with Fer' }))

// Declare a route
fastify.get('/todos', function (_req, res) {
  res.send(Array.from(db.values()))
})

fastify.post('/todos', function (req, res) {
  const todo = buildTodo(req.body)
  db.set(todo.id, todo)
  res.status(201).send(todo)
})

fastify.patch('/todos/:id', function (req, res) {
  const todo = db.get(req.params.id)
  const updatedTodo = { ...todo, ...req.body }
  db.set(todo.id, updatedTodo)
  res.send(updatedTodo)
})

fastify.delete('/todos/:id', function (req, res) {
  db.delete(req.params.id)
  res.status(204).send({})
})

fastify.listen(8080, '0.0.0.0', function (err, address) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${address}`)
})

// -- Utils

function buildTodo(attrs) {
  return {
    completed: false,
    id: uuidv4(),
    ...attrs,
  }
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = (Math.random() * 16) | 0
    // eslint-disable-next-line eqeqeq
    const v = c == 'x' ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })
}
