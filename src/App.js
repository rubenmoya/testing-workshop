import { QueryClientProvider } from 'react-query'
import { Todos } from 'screens/Todos/Todos'
import { queryClient } from 'config/queryClient'

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <Todos />
    </QueryClientProvider>
  )
}

export default App
