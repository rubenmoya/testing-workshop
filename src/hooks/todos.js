import { useMutation, useQuery } from 'react-query'
import { TodoService } from 'services/TodoService'
import { queryClient } from 'config/queryClient'

export const useFetchTodos = () => {
  const { data: todos = [], ...rest } = useQuery('todos', TodoService.fetchAll, {})

  return {
    todos,
    ...rest,
  }
}

export const useCreateTodo = (options) => {
  const { mutate: createTodo, ...rest } = useMutation((data) => TodoService.create(data), {
    onSuccess(newTodo) {
      queryClient.setQueryData('todos', (oldTodos) => [newTodo, ...oldTodos])
      options.onSuccess && options.onSuccess(newTodo)
    },
  })

  return {
    createTodo,
    ...rest,
  }
}

export const useDeleteTodo = () => {
  const { mutate: deleteTodo, ...rest } = useMutation((id) => TodoService.delete(id), {
    onSuccess(_data, id) {
      queryClient.setQueryData('todos', (oldTodos) => {
        return oldTodos.filter((oldTodo) => oldTodo.id !== id)
      })
    },
  })

  return {
    deleteTodo,
    ...rest,
  }
}

export const useUpdateTodo = () => {
  const { mutate: updateTodo, ...rest } = useMutation(({ id, data }) => TodoService.update(id, data), {
    onSuccess(updatedTodo, variables) {
      queryClient.setQueryData('todos', (oldTodos) =>
        oldTodos.map((oldTodo) => {
          return oldTodo.id === updatedTodo.id ? updatedTodo : oldTodo
        }),
      )
    },
  })

  return {
    updateTodo,
    ...rest,
  }
}
