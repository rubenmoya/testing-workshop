import { useState } from 'react'
import cx from 'classnames'
import { TodoItem } from './components/TodoItem'
import { FILTERS } from './constants'
import { useFetchTodos, useCreateTodo, useUpdateTodo, useDeleteTodo } from 'hooks/todos'

export const Todos = () => {
  const [todoTitle, setTodoTitle] = useState('')
  const [activeFilter, setActiveFilter] = useState(FILTERS.ALL)
  const { todos } = useFetchTodos()
  const { createTodo } = useCreateTodo({
    onSuccess: () => setTodoTitle(''),
  })
  const { updateTodo } = useUpdateTodo()
  const { deleteTodo } = useDeleteTodo()

  const activeTodoCount = todos.filter((todo) => !todo.completed).length
  const completedCount = todos.length - activeTodoCount

  const shownTodos = todos.filter((todo) => {
    switch (activeFilter) {
      case FILTERS.ACTIVE:
        return !todo.completed
      case FILTERS.COMPLETED:
        return todo.completed
      default:
        return true
    }
  })

  const showFooter = activeTodoCount || completedCount

  return (
    <div className="todoapp">
      <header className="header">
        <h1>Todos</h1>
        <input
          className="new-todo"
          placeholder="What needs to be done?"
          value={todoTitle}
          onKeyDown={(event) => {
            if (event.key !== 'Enter') {
              return
            }

            event.preventDefault()

            createTodo({ title: todoTitle })
          }}
          onChange={(event) => setTodoTitle(event.target.value)}
          autoFocus={true}
        />
      </header>

      {todos.length > 0 && (
        <section className="main">
          <ul className="todo-list">
            {shownTodos.map((todo) => (
              <TodoItem
                key={todo.id}
                todo={todo}
                onUpdate={(data) => updateTodo({ id: todo.id, data })}
                onDelete={() => deleteTodo(todo.id)}
              />
            ))}
          </ul>
        </section>
      )}

      {showFooter ? (
        <footer className="footer">
          <span className="todo-count">
            {`${activeTodoCount} ${activeTodoCount === 1 ? 'item' : 'items'} left`}
          </span>
          <ul className="filters">
            <li>
              <button
                onClick={() => setActiveFilter(FILTERS.ALL)}
                className={cx({ selected: activeFilter === FILTERS.ALL })}
              >
                All
              </button>
            </li>
            <li>
              <button
                onClick={() => setActiveFilter(FILTERS.ACTIVE)}
                className={cx({ selected: activeFilter === FILTERS.ACTIVE })}
              >
                Active
              </button>
            </li>
            <li>
              <button
                onClick={() => setActiveFilter(FILTERS.COMPLETED)}
                className={cx({ selected: activeFilter === FILTERS.COMPLETED })}
              >
                Completed
              </button>
            </li>
          </ul>
        </footer>
      ) : null}
    </div>
  )
}
