import fetchMock from 'fetch-mock'
import { render, screen, userEvent, waitFor } from 'tests/app-test-utils'
import { Todos } from './Todos'

const COMPLETED_TODO = { id: 'completed', title: 'Completed 1', completed: true }
const COMPLETED_TODO_2 = { id: 'completed2', title: 'Completed 2', completed: true }
const ACTIVE_TODO = { id: 'active', title: 'Active 1', completed: false }
const TODOS = [COMPLETED_TODO, COMPLETED_TODO_2, ACTIVE_TODO]

describe('Todos', () => {
  it('renders all todos by default', async () => {
    fetchMock.get('end:/todos', TODOS)
    render(<Todos />)

    expect(await screen.findByText(COMPLETED_TODO.title)).toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO_2.title)).toBeInTheDocument()
    expect(screen.getByText(ACTIVE_TODO.title)).toBeInTheDocument()
  })

  it('allows to filter by active todos', async () => {
    fetchMock.get('end:/todos', TODOS)
    render(<Todos />)

    userEvent.click(await screen.findByRole('button', { name: 'Active' }))

    expect(screen.getByText(ACTIVE_TODO.title)).toBeInTheDocument()
    expect(screen.queryByText(COMPLETED_TODO.title)).not.toBeInTheDocument()
    expect(screen.queryByText(COMPLETED_TODO_2.title)).not.toBeInTheDocument()
  })

  it('allows to filter by completed todos', async () => {
    fetchMock.get('end:/todos', TODOS)
    render(<Todos />)

    userEvent.click(await screen.findByRole('button', { name: 'Completed' }))

    expect(screen.queryByText(ACTIVE_TODO.title)).not.toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO.title)).toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO_2.title)).toBeInTheDocument()
  })

  it('allows to filter by all todos', async () => {
    fetchMock.get('end:/todos', TODOS)
    render(<Todos />)

    userEvent.click(await screen.findByRole('button', { name: 'Completed' }))

    expect(screen.queryByText(ACTIVE_TODO.title)).not.toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO.title)).toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO_2.title)).toBeInTheDocument()

    userEvent.click(await screen.findByRole('button', { name: 'All' }))

    expect(screen.getByText(ACTIVE_TODO.title)).toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO.title)).toBeInTheDocument()
    expect(screen.getByText(COMPLETED_TODO_2.title)).toBeInTheDocument()
  })

  it('allows to add a new todo', async () => {
    const title = 'My new todo'
    fetchMock.get('end:/todos', TODOS)
    fetchMock.post('end:/todos', { status: 201, body: { id: 'new', title, completed: false } })
    render(<Todos />)

    userEvent.type(screen.getByPlaceholderText('What needs to be done?'), `${title}{enter}`)

    expect(await screen.findByText(title)).toBeInTheDocument()
  })

  it('deletes a todo', async () => {
    fetchMock.get('end:/todos', TODOS)
    fetchMock.delete(`end:/todos/${ACTIVE_TODO.id}`, 204)
    render(<Todos />)

    expect(await screen.findByText(ACTIVE_TODO.title)).toBeInTheDocument()
    userEvent.click(screen.getByLabelText(`Remove ${ACTIVE_TODO.title}`))

    expect(await screen.findByText(ACTIVE_TODO.title)).not.toBeInTheDocument()
  })

  it('updates a todo', async () => {
    const newTitle = 'my new title'
    fetchMock.get('end:/todos', TODOS)
    fetchMock.patch(`end:/todos/${ACTIVE_TODO.id}`, {
      body: { ...ACTIVE_TODO, title: newTitle },
    })
    render(<Todos />)

    userEvent.dblClick(await screen.findByText(ACTIVE_TODO.title))

    const input = screen.getByDisplayValue(ACTIVE_TODO.title)
    userEvent.clear(input)
    userEvent.type(input, `${newTitle}{enter}`)

    expect(await screen.findByText(newTitle)).toBeInTheDocument()
  })

  it('allows to toggle a todo', async () => {
    fetchMock.get('end:/todos', TODOS)
    fetchMock.patch(`end:/todos/${ACTIVE_TODO.id}`, {
      body: { ...ACTIVE_TODO, completed: !ACTIVE_TODO.completed },
    })
    render(<Todos />)

    const checkbox = await screen.findByLabelText(`Toggle ${ACTIVE_TODO.title}`)

    expect(checkbox).not.toBeChecked()

    userEvent.click(checkbox)

    await waitFor(() => {
      expect(checkbox).toBeChecked()
    })
  })
})
