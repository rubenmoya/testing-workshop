import { useState, useRef, useEffect } from 'react'
import cx from 'classnames'

export const TodoItem = ({ todo, onUpdate, onDelete }) => {
  const inputRef = useRef()
  const [editing, setEditing] = useState(false)
  const [title, setTitle] = useState(todo.title)

  const onBlur = () => {
    onUpdate({ title })
    setEditing(false)
  }

  const onChange = (event) => {
    setTitle(event.target.value)
  }

  const onToggle = (event) => {
    onUpdate({ completed: event.target.checked })
    setEditing(false)
  }

  const onKeyDown = (event) => {
    if (event.key === 'Escape') {
      setEditing(false)
    }

    if (event.key === 'Enter') {
      onUpdate({ title })
      setEditing(false)
    }
  }

  useEffect(() => {
    if (editing) {
      inputRef.current.focus()
    }
  }, [editing])

  const classNames = cx({
    completed: todo.completed,
    editing,
  })

  return (
    <li className={classNames}>
      <div className="view">
        <label htmlFor={`toggle-${todo.id}`} className="visually-hidden">
          {`Toggle ${title}`}
        </label>
        <input
          id={`toggle-${todo.id}`}
          className="toggle"
          type="checkbox"
          checked={todo.completed}
          onChange={onToggle}
        />
        <span onDoubleClick={() => setEditing(true)}>{title}</span>
        <button className="destroy" onClick={onDelete} aria-label={`Remove ${title}`} />
      </div>

      {editing && (
        <input
          ref={inputRef}
          className="edit"
          value={title}
          onBlur={onBlur}
          onChange={onChange}
          onKeyDown={onKeyDown}
        />
      )}
    </li>
  )
}
