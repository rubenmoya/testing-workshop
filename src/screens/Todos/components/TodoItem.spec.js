import { render, screen, userEvent } from 'tests/app-test-utils'
import { TodoItem } from './TodoItem'

const COMPLETED_TODO = { id: 'completed', title: 'Completed', completed: true }
const ACTIVE_TODO = { id: 'active', title: 'Active', completed: false }

describe('TodoItem', () => {
  it('renders the title', () => {
    render(<TodoItem todo={COMPLETED_TODO} />)

    expect(screen.getByText(COMPLETED_TODO.title)).toBeInTheDocument()
  })

  it('renders the todo as completed', () => {
    render(<TodoItem todo={COMPLETED_TODO} />)

    const input = screen.getByLabelText('Toggle Completed')

    expect(input).toBeChecked()
  })

  it('renders the todo as active', () => {
    render(<TodoItem todo={ACTIVE_TODO} />)

    const input = screen.getByLabelText('Toggle Active')

    expect(input).not.toBeChecked()
  })

  it('calls onDelete when the delete button is pressed', () => {
    const onDelete = jest.fn()
    render(<TodoItem todo={ACTIVE_TODO} onDelete={onDelete} />)

    userEvent.click(screen.getByLabelText('Remove Active'))

    expect(onDelete).toHaveBeenCalled()
  })

  it('enters on editing mode when the title is double clicked', () => {
    render(<TodoItem todo={ACTIVE_TODO} />)

    userEvent.dblClick(screen.getByText('Active'))

    expect(screen.getByDisplayValue('Active')).toBeInTheDocument()
  })

  it('exits the editing mode when Esc is pressed', () => {
    render(<TodoItem todo={ACTIVE_TODO} />)

    userEvent.dblClick(screen.getByText('Active'))
    userEvent.type(screen.getByDisplayValue('Active'), '{escape}')

    expect(screen.queryByDisplayValue('Active')).not.toBeInTheDocument()
  })

  it('calls onUpdate when the input gets on blur', () => {
    const onUpdate = jest.fn()
    render(
      <>
        <div>Click me</div>
        <TodoItem todo={ACTIVE_TODO} onUpdate={onUpdate} />
      </>,
    )

    userEvent.dblClick(screen.getByText('Active'))

    const input = screen.queryByDisplayValue('Active')

    userEvent.type(input, ' new')
    userEvent.click(screen.getByText('Click me'))

    expect(input).not.toBeInTheDocument()
    expect(onUpdate).toHaveBeenCalledWith({ title: 'Active new' })
  })

  it('calls onUpdate when enter is pressed', () => {
    const onUpdate = jest.fn()
    render(<TodoItem todo={ACTIVE_TODO} onUpdate={onUpdate} />)

    userEvent.dblClick(screen.getByText('Active'))

    const input = screen.queryByDisplayValue('Active')

    userEvent.type(input, ' new{enter}')

    expect(input).not.toBeInTheDocument()
    expect(onUpdate).toHaveBeenCalledWith({ title: 'Active new' })
  })
})
