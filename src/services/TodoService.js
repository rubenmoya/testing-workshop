const BASE_URL = `http://localhost:8080`

export const TodoService = {
  fetchAll() {
    return fetch(`${BASE_URL}/todos`).then((res) => res.json())
  },

  create(data) {
    return fetch(`${BASE_URL}/todos`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json',
      },
    }).then((res) => res.json())
  },

  update(id, data) {
    return fetch(`${BASE_URL}/todos/${id}`, {
      method: 'PATCH',
      body: JSON.stringify(data),
      headers: {
        'content-type': 'application/json',
      },
    }).then((res) => res.json())
  },

  delete(id) {
    return fetch(`${BASE_URL}/todos/${id}`, {
      method: 'DELETE',
    })
  },
}
